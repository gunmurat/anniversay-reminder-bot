const express = require('express');
const { Client, MessageEmbed, Intents } = require('discord.js');
const dotenv = require('dotenv');
const cron = require('node-cron');

const app = express();
const client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES] });
const router = express.Router();
const port = process.env.PORT || 3000;

dotenv.config();
client.login(process.env.DISCORD_TOKEN).then(() => {
    console.log('Logged in!');
});


app.use(router);

checkAnniversary();
createExpressServer();



function checkAnniversary() {
    client.on('ready', () => {
        cron.schedule('*/5 * * * * *', async () => {
            const employees = getEmployeesFromDiscord();
            employees.forEach(member => {
                if (isAnniversaryDateForMember(member)) {
                    sendAnniversaryMessage(member);
                }
            });
        });
    });
}


function getEmployeesFromDiscord() {
    const guild = client.guilds.cache.get(process.env.DISCORD_SERVER_ID);
    const members = guild.members.cache;
    return members;

}


function isAnniversaryDateForMember(member) {
    const today = new Date();
    const joinDate = new Date(member.joinedTimestamp);
    if (joinDate.getFullYear() !== today.getFullYear() && joinDate.getMonth() === today.getMonth() && joinDate.getDate() === today.getDate()) {
        return true;
    }
    return false;
}

function calculateWorkYears(member) {
    const today = new Date();
    const joinDate = new Date(member.joinedTimestamp);
    const years = today.getFullYear() - joinDate.getFullYear();
    return years;
}

function sendAnniversaryMessage(member) {
    const embed = new MessageEmbed()
        .setTitle(":partying_face: Happy Work Anniversary! :partying_face:")
        .setDescription(`${member.displayName} has been with us for ${calculateWorkYears(member)} years!\n\nBest wishes for you.`)
        .setColor(0xF46924)
        .setImage('https://i.hizliresim.com/4jljoqr.png')
        .setTimestamp();

    const channel = client.channels.cache.get(process.env.DISCORD_CHANNEL_ID);
    channel.send({ embeds: [embed] });
}


function createExpressServer() {
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "/check");

    });

    router.get('/check', (_req, res) => {
        res.send('OK');
    });

    app.listen(port, () => {
        console.log(`Listening on port ${port}`);
    });
}
