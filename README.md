# Discord Anniversary Reminder Bot

This is an anniversary reminder bot that is developed for discord.

## USING

* Create an .env file and set 'DISCORD_CHANNEL_ID', 'DISCORD_TOKEN' and 'DISCORD_SERVER_ID' for your custom bot.

* Install dependencies
```
yarn install
```
* Run app
```
yarn start
```

![](https://gitlab.com/gunmurat/anniversay-reminder-bot/-/raw/main/assets/discord_card_looking.png)



